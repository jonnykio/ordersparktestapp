<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductVariantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'product' => new ProductResource(Product::find($this->product_id)),
            'name' => $this->name,
            'sku' => $this->sku,
            'qty' => $this->qty,
            'price' => $this->price
        ];
    }
}
