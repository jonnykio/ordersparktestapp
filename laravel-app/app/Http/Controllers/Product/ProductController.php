<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Repository\Product\ProductRepository;
use App\Utils\Response;

/**
 * @purpose
 * 
 * Handles the logical operations for products in the system
 */
class ProductController extends Controller
{
    use Response;

    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @OA\Get(
     *      path="/api/product",
     *      tags={"TESTAPP", "PRODUCT"},
     *      summary="Get list of products",
     *      description="Returns list of products",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     *     )
     */
    public function index()
    {
        return $this->productRepository->index();
    }

    /**
     * @OA\Get(
     *      path="/api/product/{id}",
     *      tags={"TESTAPP", "PRODUCT"},
     *      summary="Get product",
     *      description="Returns details of product",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     *     )
     */
    public function show($id)
    {
        return $this->productRepository->show($id);
    }

    /**
     * @OA\Post(
     *      path="/api/product",
     *      tags={"TESTAPP", "PRODUCT"},
     *      summary="Store new product",
     *      description="Returns product data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/ProductRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function store(ProductRequest $request)
    {
        return $this->productRepository->store($request);
    }

    /**
     * @OA\Put(
     *      path="/api/product/{id}",
     *      tags={"TESTAPP", "PRODUCT"},
     *      summary="Update a product",
     *      description="Updates the product data and returns the full detail.",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/ProductRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function update(ProductRequest $request, $id)
    {
        return $this->productRepository->update($request, $id);
    }

    /**
     * @OA\Delete(
     *      path="/api/product/{id}",
     *      tags={"TESTAPP", "PRODUCT"},
     *      summary="Delete product",
     *      description="Delete a product from the database",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     *     )
     */
    public function destroy($id)
    {
        return $this->productRepository->destroy($id);
    }
}
