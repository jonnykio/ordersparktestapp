<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductVariantRequest;
use App\Interfaces\ProductVariantInterface;
use App\Repository\Product\ProductVariantRepository;
use App\Utils\Response;

/**
 * @purpose
 * 
 * Handles the logical operations for product variants in the system
 */
class ProductVariantController extends Controller implements ProductVariantInterface
{
    use Response;
    
    private ProductVariantRepository $productVariantRepository;

    public function __construct(ProductVariantRepository $productVariantRepository)
    {
        $this->productVariantRepository = $productVariantRepository;
    }

    /**
     * @OA\Get(
     *      path="/api/product-variant",
     *      tags={"TESTAPP", "PRODUCTVARIANT"},
     *      summary="Get list of product variants",
     *      description="Returns list of product variants",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductVariantResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     *     )
     */
    public function index()
    {
        return $this->productVariantRepository->index();
    }
    
    /**
     * @OA\Get(
     *      path="/api/product-variant/{id}",
     *      tags={"TESTAPP", "PRODUCTVARIANT"},
     *      summary="Get product variant",
     *      description="Returns details of product variant",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductVariantResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     *     )
     */
    public function show($id)
    {
        return $this->productVariantRepository->show($id);
    }
    
    /**
     * @OA\Post(
     *      path="/api/product-variant",
     *      tags={"TESTAPP", "PRODUCTVARIANT"},
     *      summary="Store new product variant",
     *      description="Returns product variant data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/ProductVariantRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductVariantResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function store(ProductVariantRequest $request)
    {
        return $this->productVariantRepository->store($request);
    }
    
    /**
     * @OA\Put(
     *      path="/api/product-variant/{id}",
     *      tags={"TESTAPP", "PRODUCTVARIANT"},
     *      summary="Update a product variant",
     *      description="Update a product variant and return the details",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/ProductVariantRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductVariantResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function update(ProductVariantRequest $request, $id)
    {
        return $this->productVariantRepository->update($request, $id);
    }

    /**
     * @OA\Delete(
     *      path="/api/product-variant/{id}",
     *      tags={"TESTAPP", "PRODUCTVARIANT"},
     *      summary="Delete product variant",
     *      description="Delete a product variant from the database",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     *     )
     */
    public function destroy($id)
    {
        return $this->productVariantRepository->destroy($id);
    }
}
