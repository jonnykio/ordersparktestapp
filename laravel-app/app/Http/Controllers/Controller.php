<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Test App Documentation",
     *      description="Test app api documentation description",
     *      @OA\Contact(
     *          email="test@app.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="TEST APP API Server"
     * )

     *
     * @OA\Tag(
     *     name="TESTAPP",
     *     description="API Endpoints of TEST APP"
     * )
     * 
     * @OA\Tag(
     *     name="PRODUCT",
     *     description="API Endpoints of Product endpoints"
     * )
     * 
     * @OA\Tag(
     *     name="PRODUCTVARIANT",
     *     description="API Endpoints of Product variant endpoints"
     * )
     */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
