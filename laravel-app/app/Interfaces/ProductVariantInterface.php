<?php
namespace App\Interfaces;

use App\Http\Requests\ProductVariantRequest;

interface ProductVariantInterface
{
    public function index();
    public function store(ProductVariantRequest $request);
    public function show($id);
    public function update(ProductVariantRequest $request, $id);
    public function destroy($id);
}