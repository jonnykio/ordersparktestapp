<?php
namespace App\Interfaces;

use App\Http\Requests\ProductRequest;

interface ProductInterface
{
    public function index();
    public function store(ProductRequest $request);
    public function show($id);
    public function update(ProductRequest $request, $id);
    public function destroy($id);
}