<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *      title="Product variant resource",
 *      description="Product variant resource body data",
 *      type="object",
 *      required={"id", "name", "sku", "desc", "qty", "price"}
 * )
 */

class ProductVariantResource
{
    /**
     * @OA\Property(
     *      title="id",
     *      description="Id of an existing product",
     *      example="1"
     * )
     *
     * @var string
     */
    public $product_id;

    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the product variant",
     *      example="RED Product"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="sku",
     *      description="Stock Keeping Unit of the Product",
     *      example="PRD001"
     * )
     *
     * @var string
     */
    public $sku;

    /**
     * @OA\Property(
     *      title="qty",
     *      description="Quantity of the product",
     *      format="int64",
     *      example=5
     * )
     *
     * @var integer
     */
    public $qty;

    /**
     * @OA\Property(
     *      title="price",
     *      description="Product price",
     *      format="float64",
     *      example=12.99
     * )
     *
     * @var float
     */
    public $price;
}