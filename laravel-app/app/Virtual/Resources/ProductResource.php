<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *      title="Product resource",
 *      description="Product resource body data",
 *      type="object",
 *      required={"product_name", "desc", "price"}
 * )
 */

class ProductResource
{
    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the product",
     *      example="Product 001"
     * )
     *
     * @var string
     */
    public $product_name;

    /**
     * @OA\Property(
     *      title="description",
     *      description="Description of the product",
     *      example="Product Description"
     * )
     *
     * @var string
     */
    public $desc;

    /**
     * @OA\Property(
     *      title="price",
     *      description="Product price",
     *      format="float64",
     *      example=12.99
     * )
     *
     * @var float
     */
    public $price;
}