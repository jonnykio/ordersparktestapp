<?php

namespace App\Repository\Product;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Http\Resources\ProductResource;
use App\Interfaces\ProductInterface;
use App\Utils\Response;

class ProductRepository implements ProductInterface
{
    use Response;

    /**
     * Display a listing of the products.
     *
     * @return ProductResource[]
     */
    public function index()
    {
        try {
            $products = Product::all();

            return ProductResource::collection($products);
        } catch (\Throwable $th) {
            return $this->responseError(["msg" => __("There was a problem with fetching the product data, please try again."), "exception" => strval($th)], 500);
        }
    }

    /**
     * Store a newly created product
     *
     * @param  ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        try {
            $product = Product::create($request->only([
                'product_name',
                'desc',
                'price'
            ]));

            return new ProductResource($product);
        } catch (\Throwable $th) {
            return $this->responseError(["msg" => __("There was a problem with saving the product."), "exception" => strval($th)], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ProductResource
     */
    public function show($id)
    {
        try {
            $product = Product::find($id);

            return new ProductResource($product);
        } catch (\Throwable $th) {
            return $this->responseError(["msg" => __("There was a problem with retrieving the product details."), "exception" => strval($th)], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductRequest  $request
     * @param  int  $id
     * @return ProductResource
     */
    public function update(ProductRequest $request, $id)
    {
        try {
            $product = Product::find($id);
            $product->update($request->only([
                'product_name',
                'desc',
                'price'
            ]));

            return new ProductResource($product);
        } catch (\Throwable $th) {
            return $this->responseError(["msg" => __("There was a problem with updating the product."), "exception" => strval($th)], 500);
        }
    }

    /**
     * Remove the specified product.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $product = Product::find($id);

            $product->delete();

            return $this->responseData(null, __('Product deleted successfully.'));
        } catch (\Throwable $th) {
            return $this->responseError(["msg" => __("There was a problem with deleting the product."), "exception" => strval($th)], 500);
        }
    }
}
