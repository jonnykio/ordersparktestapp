<?php

namespace App\Repository\Product;

use App\Http\Requests\ProductVariantRequest;
use App\Http\Resources\ProductVariantResource;
use App\Interfaces\ProductVariantInterface;
use App\Models\ProductVariant;
use App\Utils\Response;

class ProductVariantRepository implements ProductVariantInterface
{
    use Response;

    /**
     * Display a listing of the products.
     *
     * @return ProductResource[]
     */
    public function index()
    {
        try {
            $products = ProductVariant::all();

            return ProductVariantResource::collection($products);
        } catch (\Throwable $th) {
            return $this->responseError(["msg" => __("There was a problem with fetching the product variants data, please try again."), "exception" => strval($th)], 500);
        }
    }

    /**
     * Store a newly created product variant
     *
     * @param  ProductVariantRequest  $request
     * @return ProductVariantResource
     */
    public function store(ProductVariantRequest $request)
    {
        try {
            $productVariant = ProductVariant::create($request->only([
                'product_id',
                'name',
                'sku',
                'qty',
                'price'
            ]));

            return new ProductVariantResource($productVariant);
        } catch (\Throwable $th) {
            return $this->responseError(["msg" => __("There was a problem with saving the product variant information."), "exception" => strval($th)], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ProductVariantResource
     */
    public function show($id)
    {
        try {
            $productVariant = ProductVariant::find($id);

            return new ProductVariantResource($productVariant);
        } catch (\Throwable $th) {
            return $this->responseError(["msg" => __("There was a problem with retrieving the product variant information."), "exception" => strval($th)], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductVariantRequest  $request
     * @param  int  $id
     * @return ProductVariantResource
     */
    public function update(ProductVariantRequest $request, $id)
    {
        try {
            $productVariant = ProductVariant::find($id);
            $productVariant->update($request->only([
                'product_id',
                'name',
                'sku',
                'qty',
                'price'
            ]));

            return new ProductVariantResource($productVariant);
        } catch (\Throwable $th) {
            return $this->responseError(["msg" => __("There was a problem with updating the product variant."), "exception" => strval($th)], 500);
        }
    }

    /**
     * Remove the specified product.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $productVariant = ProductVariant::find($id);

            $productVariant->delete();

            return $this->responseData(null, __('Product Variant deleted successfully.'));
        } catch (\Throwable $th) {
            return $this->responseError(["msg" => __("There was a problem with deleting the product variant."), "exception" => strval($th)], 500);
        }
    }
}
