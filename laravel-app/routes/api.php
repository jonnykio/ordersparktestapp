<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');

});

Route::middleware('auth:api')->group(function(){
    Route::APIResource('product', \App\Http\Controllers\Product\ProductController::class);
    Route::APIResource('product-variant', \App\Http\Controllers\Product\ProductVariantController::class);
});
